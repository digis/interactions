# Basic Interaction Sample

Comon **Interaface** for **Trace** interactions, ie. ray casting for object interaction.

### Video content
1) Project setup, directory structure, Blueprint names, git init, git push.
2) Interface declaration, interface application on a *cube object*, implementation of first-hit function, testing by *O-key-press*.
3) Cube extension - set material collor for different interface functions: `TraceHitObject`, `TraceLeaveObject`, `TraceActiveDown`, `TraceActiveUp`.
4) Setting up Interaction component (fce. `LineTrace`, `InteractWithHit`), implementation of `LineTrace` function.
5) Implementation of `InteractWithHit`: storing local variables, introducing collapsed nodes, old/new actor trace hit decision. Pawn extension with `TraceInteractionComponent` and simple test.
6) Implementation of `InteractWithHit`: If new object in hit, stop Trace old object, start Trace new object. Debugging LineTrace by turning visibility, enabling inner pawn-component rotation.
7) Implementation of `InteractWithHit`: Call `TraceMove` if is the same object and check if the component has changed.
8) Change state of InteractiveObject (Cube) when pressing mouse button: Added Events in `TraceInteractionComponent`, reaction on mouse click in Pawn and implementation of `TraceActiveDown/Up` in current Cube implementation (green state). Update of TraceActiveDown by random color state.
9) `TraceMove` implementation (arrow orientation). Implementation of Sphere based on Cube.


### Personal Excersise
* suggestions
  *  define other (cylinder) objects with other implementations
  *  replace color change, by different material. define colors/materials as variables. 


### Resources
* Mitch McCaffrey, Unreal Engine VR Cookbook. Addison-Wesley 2017.